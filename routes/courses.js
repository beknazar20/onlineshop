const express = require('express')
const router = express()
const Course = require('./../models/course')
const auth = require('../middleware/auth')
const {courseValidators} = require('../utils/validators')
const {validationResult} = require('express-validator')
router.get('/add-new', auth, (req,res)=>{
    res.render('add',{
        title:'Дабавит новый курс'
    })
})


router.get('/', async (req,res)=>{
    const courses = await Course
    .find()
    .lean().populate('userId')
    // .select('title price img userId')
//   console.log(courses)
    res.render('courses',{
        courses,
        title:'Все курсы'
    })
})

  
router.post('/add-new-course', auth, courseValidators, async(req,res) => {
    const errors = validationResult(req)
    if(!errors.isEmpty()){
        return res.status(422).render('add',{
            title:'Дабавить курс',
            isAdd: true,
            error:errors.array()[0].msg
        })
    }
    console.log(errors.array()[0])
    const course = new Course({
        title:req.body.title,
        price: req.body.price,
        img: req.body.img,
        userId: req.user
    })
    try {
        await course.save()
        res.redirect('/courses')
    } catch (error) {
        console.log(error)
    }
    
})



router.get('/edit-course/:id', auth, (req,res)=>{
    res.render('course',{
        title:'Редактировать курс',

    })
})

// router.get('/course-details/:id',(req,res)=>{
//     res.render('home',{
//         title:'Редактировать курс'
//     })
// })
router.get('/:id', auth, async (req,res)=>{
    const course = await Course.findById(req.params.id).lean()
    res.render('course',{
        course
    })
})

router.get('/:id/edit', auth,  async (req,res)=>{
    if(!req.query.allow) {
        return res.redirect('/')
    }
    const course = await Course.findById(req.params.id).lean()
    res.render('edit-course',{
        course,
        title: course.title
    })
})
    router.post('/edit', auth, async (req,res)=> {
        const {id} = req.body
        delete req.body.id
        await Course.findByIdAndUpdate(id,req.body)
        res.redirect('/courses')
    })

 router.post('/remove', auth, async (req,res)=> {
     try {
        await Course.deleteOne({_id: req.body.id})
        res.redirect('/courses')
     } catch (error) {
         console.log('Msg: ',error)
     }
 })   
module.exports = router
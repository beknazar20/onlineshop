// const fs = require('fs')
// const path = require('path')
// const { uuid } = require('uuidv4');

// const p = path.join(__dirname,'..','data', 'courses.json')

// class Course {
//     constructor(title,price,img){
//         this.title = title,
//         this.price = price,
//         this.img = img,
//         this.id = uuid()
//     }

//     toJSON() {
//         return {
//           title: this.title,
//           price: this.price,
//           img: this.img,
//           id: this.id
//         }
//       }

//     static getAll(){
//         return new Promise((resolve,reject)=>{
//             fs.readFile(p,'utf-8',(err,content)=>{
//                 if(err){
//                     reject(err)
//                 }else{
//                     resolve(JSON.parse(content))
//                 }
//             })
//         })
//     }
//     async save() {
//         let courses = await Course.getAll()
//         courses.push(this.toJSON());
//         console.log(courses)
//         return new Promise((resolve, reject) => {
//           fs.writeFile(
//             path.join(__dirname, '..', 'data', 'courses.json'),
//             JSON.stringify(courses),
//             (err) => {
//               if (err) {
//                 reject(err)
//               } else {
//                 resolve()
//               }
//             }
//           )
//         })
//       }
//       static async getById(id){
//         const courses = await Course.getAll()
//         return courses.find(c => c.id === id)
//       }

      
//       static async update(course){
//         const courses = await Course.getAll()
//         const idx = courses.findIndex(c => c.id === course.id)
//         courses[idx] = course

//         return new Promise((resolve, reject) => {
//           fs.writeFile(
//             path.join(__dirname, '..', 'data', 'courses.json'),
//             JSON.stringify(courses),
//             (err) => {
//               if (err) {
//                 reject(err)
//               } else {
//                 resolve()
//               }
//             }
//           )
//         })
//       }
// }
// module.exports = Course

const {Schema, model} = require('mongoose')

const course = new Schema({
  title: {
    required:true,
    type:String
  },
  price: {
    type: Number,
    required:true
  },
  img: String,
  userId : {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  }
})

course.method('toClient', function(){
  const course = this.toObject()
  course.id = course._id
  delete course._id
  return course
})
module.exports = model('Course',course)
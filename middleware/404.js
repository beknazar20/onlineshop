module.exports = function(rec,res,next){
    res.status(404).render('404',{
        title:'Страница не найдено'
    })
}
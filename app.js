const express = require('express')
const app = express()
// const sendGridApi ='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'
//Routes
const homePage = require('./routes/home')
const coursesPage = require('./routes/courses')
const cardPage = require('./routes/card')
const authRoute = require('./routes/auth')
const ordersRoutes = require('./routes/orders')
const profileRoutes = require('./routes/profile')

//Middlevares
const varMiddleware = require('./middleware/variables')
const userMiddleware = require('./middleware/user')
const erroeHandler = require('./middleware/404')
const fileMiddleware = require('./middleware/file')

const path = require('path')
const dotenv = require('dotenv');
const bodyParser = require('body-parser');
const session = require('express-session')
const mongoose = require('mongoose')
const flash = require('connect-flash')
const { allowInsecurePrototypeAccess} = require('@handlebars/allow-prototype-access')
const remoteURL = 'mongodb+srv://Beknazar:O38LM5bKyxDF05S2@shop-kuqtq.mongodb.net/courseApp3';
app.use(bodyParser.urlencoded({
    extended: false
}));
const MongoStore = require('connect-mongodb-session')(session)
const store = new MongoStore({
    collection: 'sessions',
    uri: remoteURL
})

app.use(express.json())

dotenv.config({
    path: './config.env'
});
const exphbs = require('express-handlebars')
app.use(express.static(__dirname + "/public"));
app.use('/images', express.static(__dirname + "/images"));
app.use(session({
    secret: 'Some secret value',
    resave: false,
    saveUninitialized: false,
    store
}))
app.use(flash())
app.use(varMiddleware)
app.use(userMiddleware)

app.use(fileMiddleware.single('avatar'))
//Connect with Handebars
const Handlebars = require('handlebars');
const hbs = exphbs.create({
    defaultLayout: 'main',
    extname: 'hbs',
    handlebars: allowInsecurePrototypeAccess(Handlebars)
});

app.engine('hbs', hbs.engine)
app.set('view engine', 'hbs')
app.set('views', 'views')


app.use('/', homePage)
app.use('/courses', coursesPage)
app.use('/card', cardPage)
app.use('/auth', authRoute)
app.use('/orders', ordersRoutes)
app.use('/profile', profileRoutes)
app.use(erroeHandler)

async function start() {
    const PORT = process.env.PORT
    try {
        mongoose.connect(remoteURL, {
            useCreateIndex: true,
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        })

        app.listen(PORT, () => console.log(`Server started on port ${PORT}`))
    } catch (error) {
        console.log(error)
    }
}
start()